//
//  ViewController.swift
//  TestWebviewPreload
//
//  Created by Ricardo Rautalahti-Hazan on 26/04/2019.
//  Copyright © 2019 Carioca Consulting Oy. All rights reserved.
//

import WebKit
import UIKit

class ViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    private let webViewA: WKWebView = WKWebView(frame: UIScreen.main.bounds)
    private let webViewB: WKWebView = WKWebView(frame: UIScreen.main.bounds)
    private let webViewC: WKWebView = WKWebView(frame: UIScreen.main.bounds)
    
    required init?(coder aDecoder: NSCoder) {
        self.webViewA.load(URLRequest(url: URL(string:"about:blank")!))
        self.webViewB.load(URLRequest(url: URL(string:"about:blank")!))
        self.webViewC.load(URLRequest(url: URL(string:"about:blank")!))
     
        
        super.init(coder: aDecoder)
    }
    
    private weak var pageController : UIPageViewController?
    
    private var flowControllers = NSHashTable<ContentViewController1>()
    
    private var articleFlowUrls: [URL] = [
        URL(string:"https://www.iltalehti.fi/kotimaa/a/dcc6d86f-fa54-4cd1-a08f-a24fc1e5d222")!,
        URL(string:"https://www.iltalehti.fi/politiikka/a/a8c2394b-d21d-4b89-8355-a69d44f69255")!,
        URL(string:"https://www.iltalehti.fi/kotimaa/a/9db73c90-6072-45ef-aa89-fd41917d8902")!,
        URL(string:"https://www.iltalehti.fi/ulkomaat/a/706d62f7-4245-4bc1-a4f2-649bf5ac5337")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006084403")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083323")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083297")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083393")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083164")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083259")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083157")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083511")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083413")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083443")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083323")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083297")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083393")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083164")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083259")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083157")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083511")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083413")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083443")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083323")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083297")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083393")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083164")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083259")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083157")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083511")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083413")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083443")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083323")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083297")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083393")!,
        URL(string:"https://mal.iltasanomat.fi/v1/articles/2000006083164")!,
    ]
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let contentController = viewController as! ContentViewController1
        print(#function, contentController.flowIndex - 1)
        return getFlowContent(flowIndex: contentController.flowIndex - 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let contentController = viewController as! ContentViewController1
        print(#function, contentController.flowIndex + 1)

        return getFlowContent(flowIndex: contentController.flowIndex + 1)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //createPages()
        
        self.view.addSubview(self.webViewA)
        self.view.addSubview(self.webViewB)
        self.view.addSubview(self.webViewC)
      
        
        let pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageController.dataSource = self
        pageController.delegate = self
        
        pageController.view.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: self.view.frame.height)
        self.addChild(pageController)
        self.view.addSubview(pageController.view)
        pageController.didMove(toParent: self)
        
        let vc = UIViewController()
        vc.view.backgroundColor = .red
        
        pageController.setViewControllers([getFlowContent(flowIndex: 0)!], direction: .forward, animated: false, completion: nil)
        
        self.pageController = pageController
    }
    
    private func getFlowContent(flowIndex: Int) -> ContentViewController1? {
        if 0...self.articleFlowUrls.count-1 ~= flowIndex {
            //warmFlowContent(flowIndex: flowIndex)
            return cachedControllerFor(flowIndex: flowIndex)
        }
        return nil
    }
    
//    private func warmFlowContent(flowIndex: Int) {
//        let nextIndex = flowIndex + 1
//        if 0...self.articleFlowUrls.count-1 ~= nextIndex {
//            let vc = cachedControllerFor(flowIndex: nextIndex)
//            if !vc.preloaded, let nextView = vc.view {
//                // self.addChild(vc)
//
//
//                nextView.frame.origin.x = nextView.frame.size.width * -1000
//                self.pageController?.view.addSubview(nextView)
//            }
//        }
//
//        let nextNextIndex = flowIndex + 2
//        if 0...self.articleFlowUrls.count-1 ~= nextNextIndex {
//            let vc = cachedControllerFor(flowIndex: nextNextIndex)
//            if !vc.preloaded, let nextView = vc.view {
//                // self.addChild(vc)
//
//
//                nextView.frame.origin.x = nextView.frame.size.width * -1000
//                self.pageController?.view.addSubview(nextView)
//            }
//        }
//
//
//        let previousIndex = flowIndex - 1
//        if 0...self.articleFlowUrls.count-1 ~= previousIndex {
//            let vc = cachedControllerFor(flowIndex: previousIndex)
//            if !vc.preloaded, let nextView = vc.view {
//                // self.addChild(vc)
//
//
//                nextView.frame.origin.x = nextView.frame.size.width * -1000
//                self.pageController?.view.addSubview(nextView)
//            }
//        }
//
//        let previousPreviousIndex = flowIndex - 2
//        if 0...self.articleFlowUrls.count-1 ~= previousPreviousIndex {
//            let vc = cachedControllerFor(flowIndex: previousPreviousIndex)
//            if !vc.preloaded, let nextView = vc.view {
//                // self.addChild(vc)
//
//
//                nextView.frame.origin.x = nextView.frame.size.width * -1000
//                self.pageController?.view.addSubview(nextView)
//            }
//        }
//    }
    
    private func cachedControllerFor(flowIndex: Int) -> ContentViewController1 {
        //        let cachedObjects = flowControllers.allObjects
        //        let index = flowControllers.allObjects.firstIndex { (flowController) -> Bool in
        //            return flowController.flowIndex == flowIndex
        //        }
        //
        //        debugPrint("found index:", index, "for flow index: ", flowIndex)
        //        if let index = index {
        //            return cachedObjects[index]
        //        } else {
        
        
        var webView: WKWebView
        
        switch flowIndex % 3 {
        case 0: webView = self.webViewA
        case 1: webView = self.webViewB
        case 2: webView = self.webViewC
        default: webView = self.webViewA
        }
        
         //webView.load(URLRequest(url: URL(string:"about:blank")!))
        webView.evaluateJavaScript("document.body.remove()")
        if webView.superview != nil {           
            webView.setNeedsDisplay()
            webView.removeFromSuperview()
        }
        webView.load(URLRequest(url: articleFlowUrls[flowIndex]))
        let controller = ContentViewController1(flowIndex: flowIndex, webView: webView)
        flowControllers.add(controller)
        return controller
        //}
    }
}

