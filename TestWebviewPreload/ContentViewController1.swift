//
//  ContentViewController1.swift
//  TestWebviewPreload
//
//  Created by Ricardo Rautalahti-Hazan on 26/04/2019.
//  Copyright © 2019 Carioca Consulting Oy. All rights reserved.
//

import UIKit
import WebKit

class ContentViewController1: UIViewController {

    let flowIndex: Int
    
    private weak var webView: WKWebView?
    
    init(flowIndex: Int, webView: WKWebView) {
        self.flowIndex = flowIndex
        super.init(nibName: nil, bundle: nil)
        view.addSubview(webView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(#function, "flow index:", flowIndex)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
